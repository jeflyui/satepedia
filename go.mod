module github.com/heroku/sate-pedia

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/antchfx/htmlquery v1.0.0 // indirect
	github.com/antchfx/xmlquery v1.0.0 // indirect
	github.com/antchfx/xpath v0.0.0-20190319080838-ce1d48779e67 // indirect
	github.com/gin-gonic/gin v0.0.0-20150626140855-4cc2de6207f4
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/heroku/x v0.0.0-20171004170240-705849e307dd
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/manucorporat/sse v0.0.0-20150604091100-c142f0f1baea // indirect
	github.com/mattn/go-colorable v0.0.0-20150625154642-40e4aedc8fab // indirect
	github.com/mattn/go-isatty v0.0.0-20150814002629-7fcbc72f853b // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/temoto/robotstxt v0.0.0-20180810133444-97ee4a9ee6ea // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/appengine v1.6.0 // indirect
	gopkg.in/bluesuncorp/validator.v5 v5.9.1 // indirect
)
