package crawl

import (
	"fmt"
	"log"
	"time"

	"github.com/gocolly/colly"
)

var latestDate string
var article ArticleURL

func GetRecentArticleURL() ArticleURL {
	c := colly.NewCollector()

	year, month, day := time.Now().Date()
	currentDate := fmt.Sprintf("%d-%d-%d", year, int(month), day)
	if currentDate == latestDate {
		return article
	}
	c.OnError(func(r *colly.Response, e error) {
		log.Println("error:", e, r.Request.URL, string(r.Body))
	})
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String(), "on", currentDate)
	})
	c.OnHTML("#recent-posts-3", func(e *colly.HTMLElement) {
		e.ForEach("a", func(i int, elem *colly.HTMLElement) {
			if i == 0 {
				article = ArticleURL{
					URL:   elem.Attr("href"),
					Title: elem.Text,
				}
				latestDate = currentDate
			}
		})
	})
	c.Visit("http://www.warungsatekamu.org")
	return article
}
