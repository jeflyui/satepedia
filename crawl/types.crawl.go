package crawl

type ArticleURL struct {
	Title string `json:"title"`
	URL   string `json:"url"`
}
