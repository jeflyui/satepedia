package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/heroku/sate-pedia/crawl"
)

//index handler
func Index(c *gin.Context) {
	index := map[string]interface{}{
		"appname":     "satepedia",
		"description": "saat teduh for everyone needs",
		"status":      "running",
	}
	c.JSON(http.StatusOK, index)
}

//for slack bot
func SlackSlash(c *gin.Context) {
	article := crawl.GetRecentArticleURL()
	response := map[string]interface{}{
		"text": fmt.Sprintf("%s - %s", article.Title, article.URL),
	}
	c.JSON(http.StatusOK, response)
}
