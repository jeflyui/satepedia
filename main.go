package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/heroku/sate-pedia/handler"
	_ "github.com/heroku/x/hmetrics/onload"
)

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.Use(gin.Logger())

	router.GET("/", handler.Index)
	router.POST("/slack/slash", handler.SlackSlash)

	log.Println("Running app on port :", port)
	router.Run(":" + port)
}
